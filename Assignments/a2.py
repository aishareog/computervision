# import cv2
# import time
# import numpy as np
# import math

# Use find contour if all else fails
# l12 = x1*x2

# # Part not 2
# def main():
#     cap = cv2.VideoCapture(0)
#     print('test')

#     while(True):
#         ret, frame = cap.read()
        

#         t_lower = 100  # Lower Threshold
#         t_upper = 150  # Upper threshold


#         # Applying the Canny Edge filter
#         edge = cv2.Canny(frame, t_lower, t_upper)
#         # Copy edges to the images that will display the results in BGR
#         cdst = cv2.cvtColor(edge, cv2.COLOR_GRAY2BGR)
#         cdstP = np.copy(cdst)

#         lines = cv2.HoughLines(edge, 1, np.pi / 180, 150, None, 0, 0)

#         if lines is not None:
#             for i in range(0, len(lines)):
#                 rho = lines[i][0][0]
#                 theta = lines[i][0][1]
#                 a = math.cos(theta)
#                 b = math.sin(theta)
#                 x0 = a * rho
#                 y0 = b * rho
#                 pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
#                 pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
#                 cv2.line(cdst, pt1, pt2, (0,0,255), 3, cv2.LINE_AA)

#         linesP = cv2.HoughLinesP(edge, 1, np.pi / 180, 50, None, 50, 10)

#         if linesP is not None:
#             for i in range(0, len(linesP)):
#                 l = linesP[i][0]
#                 cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0,0,255), 3, cv2.LINE_AA)


#         frame = cv2.flip(frame,1)
#         edge = cv2.flip(edge,1)
#         cdst = cv2.flip(cdst,1)
#         cdstP = cv2.flip(cdstP,1)
#         cv2.imshow('frame',frame)
#         cv2.imshow('edge',edge)
#         cv2.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)
#         cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             break

#     cap.release()
#     cv2.destroyAllWindows()



# main()



# RANSAC  - Functional with image
# import numpy as np
# import cv2 
# from matplotlib import pyplot as plt
# img = cv2.imread('Assignments/sampleLine.jpg',0)
# edges = cv2.Canny(img,100,200)

# points = np.argwhere(edges != 0)


# iterations = 0
# inliers = 0
# max_inliers = 0 


# k = 10
# t = 0.05  
# d = 3 # Minimum distance to classify as inlier
# edge1 = 0
# edge2 = 0
# while iterations <= k:
    
#     # Acqiuring 2 points
#     if(len(points)==0): # If nothing is detected
#         p1 = (0,0)
#         p2 = (0,0)
#     else:               # Selecting 2 random values in detected edge
#         p1 = points[np.random.randint(0,len(points))]
#         p2 = points[np.random.randint(0,len(points))]


#     for point in points:
    
#         # Calculating distance from each point
#         # https://stackoverflow.com/questions/39840030/distance-between-point-and-a-line-from-two-points
#         dist = np.linalg.norm(np.cross(p2-p1, p1-point))/np.linalg.norm(p2-p1)
#         # print(dist)
#         if (dist < d):
#             inliers += 1

#     if inliers > max_inliers:
#         max_inliers = inliers
#         edge1 = p1 
#         edge2 = p2

#     print(inliers)
    
#     inliers = 0
#     iterations += 1

# print(str(edge1)+', '+ str(edge2))
# print(max_inliers)



# edges = cv2.line(edges,(edge1[1],edge1[0]),(edge2[1],edge2[0]),(255, 0, 0), 2)
# plt.subplot(121),plt.imshow(img,cmap = 'gray')
# plt.title('Original Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122),plt.imshow(edges,cmap = 'gray')
# plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
# plt.show()







import numpy as np
import cv2 
from matplotlib import pyplot as plt
# img = cv2.imread('Assignments/sampleLine.jpg',0)
cap = cv2.VideoCapture(0)
while(True):
    ret, frame = cap.read()

    img = frame[100:480, 0:640]
    
    edges = cv2.Canny(img,100,200)

    points = np.argwhere(edges != 0)


    iterations = 0
    inliers = 0
    max_inliers = 0 


    k = 5
    t = 0.05  
    d = 3 # Minimum distance to classify as inlier
    edge1 = np.array([0,0])
    edge2 = np.array([0,0])
    while iterations <= k:
        
        # Acqiuring 2 points
        if(len(points)==0): # If nothing is detected
            p1 = np.array([0,0])
            p2 = np.array([0,0])
        else:               # Selecting 2 random values in detected edge
            p1 = points[np.random.randint(0,len(points))]
            p2 = points[np.random.randint(0,len(points))]


        for point in points:
        
            # Calculating distance from each point
            # https://stackoverflow.com/questions/39840030/distance-between-point-and-a-line-from-two-points
            dist = np.linalg.norm(np.cross(p2-p1, p1-point))/np.linalg.norm(p2-p1)
            # print(dist)
            if (dist < d):
                inliers += 1

        if inliers > max_inliers:
            max_inliers = inliers
            edge1 = p1 
            edge2 = p2

        # print(inliers)
        
        inliers = 0
        iterations += 1

    # print(str(edge1)+', '+ str(edge2))
    # print(max_inliers)



    edges = cv2.line(edges,(edge1[1],edge1[0]),(edge2[1],edge2[0]),(255,0,0), 2)
    # plt.subplot(121),plt.imshow(img,cmap = 'gray')
    # plt.title('Original Image'), plt.xticks([]), plt.yticks([])
    # plt.subplot(122),plt.imshow(edges,cmap = 'gray')
    # plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
    # plt.show()


    cv2.imshow('frame',img)
    cv2.imshow('edge',edges)
    # cv2.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)
    # cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
